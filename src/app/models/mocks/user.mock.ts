import { JsonPipe } from '@angular/common';
import User from '@models/user.model';

const aUser: User = {
    id: 1,
    firstName: "mari",
    lastName: "t-j",
    email: "an@email.com",
    dob: new Date(1995, 11, 17),
    role: "Admin, Contributor",
    isContributor: false,
    isAdmin: false,
    profile: {
        id: 1,
        weight: 78,
        height: 180,
        medicalConditions: "",
        disabilities: "",
        address: {
            id: 1,
            addressLine1: "an address 4",
            addressLine2: "",
            addressLine3: "",
            postalCode: 1334,
            city: "Oslo",
            country: "Norway"
        },
    },
    gender: {
        id: 0,
        name: "Female"
    }
};
const bUser: User = JSON.parse(JSON.stringify(aUser));
const cUser: User = JSON.parse(JSON.stringify(aUser));
const dUser: User = JSON.parse(JSON.stringify(aUser));

bUser.isAdmin = true;
bUser.profile.id = 2;
bUser.id = 2;
bUser.lastName = "nr2";
cUser.isContributor = true;
cUser.profile.id = 3;
cUser.id = 3;
cUser.lastName = "the third";
dUser.isAdmin = true;
dUser.isContributor = true;
dUser.profile.id = 4;
dUser.id = 4;
dUser.lastName = "o";

let userList: User[] = [aUser, bUser, cUser, dUser];

export default aUser;
export { userList };