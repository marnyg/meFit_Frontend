import Program from "@models/program.model";
import categoryList from "@models/mocks/category-mocks";
import programWorkoutList from '@models/mocks/program-workout-mocks';

let [workout1, workout2, workout3, workout4, ...rest] = programWorkoutList;
let [cat1, cat2, cat3, cat4] = categoryList;

let programList: Program[] = [
    { id: 1, name: "Mock Program 1", category: [cat1, cat2], workouts: [workout1, workout2, workout3, workout4] },
    { id: 2, name: "Mock Program 2", category: [cat2, cat3], workouts: [workout2, workout3] },
    { id: 3, name: "Mock Program 3", category: [cat1, cat4], workouts: [workout3, workout4] },
    { id: 4, name: "Mock Program 4", category: [cat1, cat3], workouts: [workout1, workout2] },
    { id: 5, name: "Mock Program 5", category: [cat3, cat4], workouts: [workout1, workout2] },
    { id: 6, name: "Mock Program 6", category: [cat1], workouts: [workout1] },
    { id: 7, name: "Mock Program 7", category: [cat2], workouts: [workout2] },
    { id: 8, name: "Mock Program 8", category: [cat3], workouts: [workout3] },
    { id: 9, name: "Mock Program 9", category: [cat3], workouts: [workout4] },
];
// let programList: Program[] = [
//     { id: 1, name: "Mock Program 1", workouts: [workout1, workout2, workout3, workout4] },
//     { id: 2, name: "Mock Program 2", workouts: [workout2, workout3] },
//     { id: 3, name: "Mock Program 3", workouts: [workout3, workout4] },
//     { id: 4, name: "Mock Program 4", workouts: [workout1, workout2] },
//     { id: 5, name: "Mock Program 5", workouts: [workout1, workout2] },
//     { id: 6, name: "Mock Program 6", workouts: [workout1] },
//     { id: 7, name: "Mock Program 7", workouts: [workout2] },
//     { id: 8, name: "Mock Program 8", workouts: [workout3] },
//     { id: 9, name: "Mock Program 9", workouts: [workout4] },
// ];
export default programList;