
export default interface PutUser {
    profile: {
        address: {
            addressLine1: string,
            country: string,
            postalCode: string,
            city: string
        },
        genderId: number,
        weight: number,
        height: number,
        medicalConditions?: string,
        disabilities?: string
    },
    firstName: string,
    lastName: string,
    dob: Date
}