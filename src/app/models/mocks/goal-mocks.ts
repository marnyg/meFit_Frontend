import Goal from "@models/goal.model";
import * as workoutMocks from '@models/mocks/workout-mocks';
import programList from '@models/mocks/program-mocks';
import goalWorkouList from '@models/mocks/goal-workout-mocks';
import goalWorkoutList from '@models/mocks/goal-workout-mocks';

// let [workout1, workout2, workout3, workout4] = workoutMocks.workoutList;
let [goalWorkou1, goalWorkou2, goalWorkou3, goalWorkou4, goalWorkou5,] = goalWorkoutList;
let [prog1, prog2] = programList;

function dateOffsetFromToday(numDaysOffsett: number) {
    var today = new Date();
    var nextweek = new Date(today.getFullYear(), today.getMonth(), today.getDate() + numDaysOffsett);
    return nextweek;
}

let goalList: Goal[] = [
    { id: 1, isActive: true, isCompleted: false, profileId: 1, startDate: dateOffsetFromToday(-2), goalWorkouts: [goalWorkou1, goalWorkou2], program: prog1 },
    { id: 2, isActive: false, isCompleted: false, profileId: 1, startDate: new Date(), goalWorkouts: [goalWorkou2, goalWorkou5], program: prog2 },
    { id: 3, isActive: false, isCompleted: true, profileId: 1, startDate: dateOffsetFromToday(-7), goalWorkouts: [goalWorkou3, goalWorkou4], program: null },
    { id: 4, isActive: false, isCompleted: true, profileId: 2, startDate: dateOffsetFromToday(-14), goalWorkouts: null, program: prog2 }
];

export default goalList;