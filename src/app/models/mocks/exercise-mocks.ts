import Exercise from "@models/exercise.model";
import categoryList from "@models/mocks/category-mocks";

let [cat1, cat2, cat3, cat4] = categoryList;

let exerciseList: Exercise[] = [
    {
        id: 1,
        name: "Mock Exercise 1",
        description: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Accusantium consequuntur officia quis laboriosam? Quaerat aut unde quia officiis. Cum repellendus veritatis eos vero nihil iure iste blanditiis praesentium ratione! Rerum!',
        categories: [cat1, cat2],
        image: new URL("https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2F4.bp.blogspot.com%2F_jMk45ZKRwH8%2FTP3FTAcJb6I%2FAAAAAAAAAAU%2FIV-74-DWpOI%2Fs1600%2FR6XLZVCALODOQTCAEH65QQCAKO4WM9CA8FBVGZCAQRQ3ZFCANSOIBTCAYAX8Y0CAVN2FTRCATJX4UICAB5E732CA0I44N9CAC52JL1CA35RZACCA2MA7CYCAZMG83YCA7RJT5HCAEQNETQCA8A21F8.jpg&f=1&nofb=1"),
        video: new URL("https://www.youtube.com/watch?v=N0JYbAerWdo")
    },
    {
        id: 2,
        name: "Mock Exercise 2",
        description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veritatis et assumenda aspernatur odio. Ratione unde quis exercitationem inventore, suscipit iure, enim ex et nemo eveniet neque, minus consectetur facilis animi?',
        categories: [cat1, cat2, cat3],
        image: new URL("https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2F4.bp.blogspot.com%2F_jMk45ZKRwH8%2FTP3FTAcJb6I%2FAAAAAAAAAAU%2FIV-74-DWpOI%2Fs1600%2FR6XLZVCALODOQTCAEH65QQCAKO4WM9CA8FBVGZCAQRQ3ZFCANSOIBTCAYAX8Y0CAVN2FTRCATJX4UICAB5E732CA0I44N9CAC52JL1CA35RZACCA2MA7CYCAZMG83YCA7RJT5HCAEQNETQCA8A21F8.jpg&f=1&nofb=1"),
        video: new URL("https://www.youtube.com/watch?v=N0JYbAerWdo")
    },
    {
        id: 3,
        name: "Mock Exercise 3",
        description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorem, perspiciatis eos. Eius inventore quod, doloremque error voluptatem quaerat molestias facilis, delectus perspiciatis hic magnam enim, sed quae aperiam voluptas nulla?',
        categories: [cat3, cat2],
        image: new URL("https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2F4.bp.blogspot.com%2F_jMk45ZKRwH8%2FTP3FTAcJb6I%2FAAAAAAAAAAU%2FIV-74-DWpOI%2Fs1600%2FR6XLZVCALODOQTCAEH65QQCAKO4WM9CA8FBVGZCAQRQ3ZFCANSOIBTCAYAX8Y0CAVN2FTRCATJX4UICAB5E732CA0I44N9CAC52JL1CA35RZACCA2MA7CYCAZMG83YCA7RJT5HCAEQNETQCA8A21F8.jpg&f=1&nofb=1"),
        video: new URL("https://www.youtube.com/watch?v=N0JYbAerWdo")
    },
    {
        id: 4,
        name: "Mock Exercise 4",
        description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Ad fugiat expedita quasi non. Totam voluptatem atque est recusandae inventore ipsam, excepturi eum, numquam officiis possimus delectus praesentium repellendus, impedit laborum.',
        categories: [cat1, cat4],
        image: new URL("https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2F4.bp.blogspot.com%2F_jMk45ZKRwH8%2FTP3FTAcJb6I%2FAAAAAAAAAAU%2FIV-74-DWpOI%2Fs1600%2FR6XLZVCALODOQTCAEH65QQCAKO4WM9CA8FBVGZCAQRQ3ZFCANSOIBTCAYAX8Y0CAVN2FTRCATJX4UICAB5E732CA0I44N9CAC52JL1CA35RZACCA2MA7CYCAZMG83YCA7RJT5HCAEQNETQCA8A21F8.jpg&f=1&nofb=1"),
        video: new URL("https://www.youtube.com/watch?v=N0JYbAerWdo")
    },
    {
        id: 5,
        name: "Mock Exercise 5",
        description: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ipsam earum veritatis repellendus voluptates accusamus dolor consequatur error incidunt nulla repellat blanditiis, odit fugit, molestias sint laudantium minima odio cumque distinctio.',
        categories: [cat1, cat2, cat3, cat4],
        image: new URL("https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2F4.bp.blogspot.com%2F_jMk45ZKRwH8%2FTP3FTAcJb6I%2FAAAAAAAAAAU%2FIV-74-DWpOI%2Fs1600%2FR6XLZVCALODOQTCAEH65QQCAKO4WM9CA8FBVGZCAQRQ3ZFCANSOIBTCAYAX8Y0CAVN2FTRCATJX4UICAB5E732CA0I44N9CAC52JL1CA35RZACCA2MA7CYCAZMG83YCA7RJT5HCAEQNETQCA8A21F8.jpg&f=1&nofb=1"),
        video: new URL("https://www.youtube.com/watch?v=N0JYbAerWdo")
    },
    {
        id: 6,
        name: "Mock Exercise 6",
        description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Fugit provident voluptas, autem alias odio iure dolores earum quaerat dolor harum minus eveniet nulla, sapiente cupiditate. Et cumque explicabo fuga. Nisi?',
        categories: [cat1, cat2],
        image: new URL("https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2F4.bp.blogspot.com%2F_jMk45ZKRwH8%2FTP3FTAcJb6I%2FAAAAAAAAAAU%2FIV-74-DWpOI%2Fs1600%2FR6XLZVCALODOQTCAEH65QQCAKO4WM9CA8FBVGZCAQRQ3ZFCANSOIBTCAYAX8Y0CAVN2FTRCATJX4UICAB5E732CA0I44N9CAC52JL1CA35RZACCA2MA7CYCAZMG83YCA7RJT5HCAEQNETQCA8A21F8.jpg&f=1&nofb=1"),
        video: new URL("https://www.youtube.com/watch?v=N0JYbAerWdo")
    },
    {
        id: 7,
        name: "Mock Exercise 7",
        description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident eum ullam, doloribus placeat explicabo ipsum quam, iusto dolorum obcaecati eos neque, ipsam esse hic suscipit saepe rem. Iste, facere perspiciatis?',
        categories: [cat1, cat2],
        image: new URL("https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2F4.bp.blogspot.com%2F_jMk45ZKRwH8%2FTP3FTAcJb6I%2FAAAAAAAAAAU%2FIV-74-DWpOI%2Fs1600%2FR6XLZVCALODOQTCAEH65QQCAKO4WM9CA8FBVGZCAQRQ3ZFCANSOIBTCAYAX8Y0CAVN2FTRCATJX4UICAB5E732CA0I44N9CAC52JL1CA35RZACCA2MA7CYCAZMG83YCA7RJT5HCAEQNETQCA8A21F8.jpg&f=1&nofb=1"),
        video: new URL("https://www.youtube.com/watch?v=N0JYbAerWdo")
    },
    {
        id: 8,
        name: "Mock Exercise 8",
        description: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Itaque expedita doloribus dignissimos quo? Ullam, odio consequuntur perferendis, error sint sit maiores repellendus sapiente possimus dolorem eum laudantium ad pariatur et!',
        categories: [cat1, cat2],
        image: new URL("https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2F4.bp.blogspot.com%2F_jMk45ZKRwH8%2FTP3FTAcJb6I%2FAAAAAAAAAAU%2FIV-74-DWpOI%2Fs1600%2FR6XLZVCALODOQTCAEH65QQCAKO4WM9CA8FBVGZCAQRQ3ZFCANSOIBTCAYAX8Y0CAVN2FTRCATJX4UICAB5E732CA0I44N9CAC52JL1CA35RZACCA2MA7CYCAZMG83YCA7RJT5HCAEQNETQCA8A21F8.jpg&f=1&nofb=1"),
        video: new URL("https://www.youtube.com/watch?v=N0JYbAerWdo")
    },
    {
        id: 9,
        name: "Mock Exercise 9",
        description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veritatis et assumenda aspernatur odio. Ratione unde quis exercitationem inventore, suscipit iure, enim ex et nemo eveniet neque, minus consectetur facilis animi?',
        categories: [cat1, cat2],
        image: new URL("https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2F4.bp.blogspot.com%2F_jMk45ZKRwH8%2FTP3FTAcJb6I%2FAAAAAAAAAAU%2FIV-74-DWpOI%2Fs1600%2FR6XLZVCALODOQTCAEH65QQCAKO4WM9CA8FBVGZCAQRQ3ZFCANSOIBTCAYAX8Y0CAVN2FTRCATJX4UICAB5E732CA0I44N9CAC52JL1CA35RZACCA2MA7CYCAZMG83YCA7RJT5HCAEQNETQCA8A21F8.jpg&f=1&nofb=1"),
        video: new URL("https://www.youtube.com/watch?v=N0JYbAerWdo")
    },
    {
        id: 10,
        name: "Mock Exercise 10",
        description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veritatis et assumenda aspernatur odio. Ratione unde quis exercitationem inventore, suscipit iure, enim ex et nemo eveniet neque, minus consectetur facilis animi?',
        categories: [cat1, cat2],
        image: new URL("https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2F4.bp.blogspot.com%2F_jMk45ZKRwH8%2FTP3FTAcJb6I%2FAAAAAAAAAAU%2FIV-74-DWpOI%2Fs1600%2FR6XLZVCALODOQTCAEH65QQCAKO4WM9CA8FBVGZCAQRQ3ZFCANSOIBTCAYAX8Y0CAVN2FTRCATJX4UICAB5E732CA0I44N9CAC52JL1CA35RZACCA2MA7CYCAZMG83YCA7RJT5HCAEQNETQCA8A21F8.jpg&f=1&nofb=1"),
        video: new URL("https://www.youtube.com/watch?v=N0JYbAerWdo")
    },
];
let singleExercise = exerciseList[0];

export { exerciseList, singleExercise };