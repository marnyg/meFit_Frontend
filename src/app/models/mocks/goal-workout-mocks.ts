import GoalWorkout from "@models/goal-workout.model";
import * as workoutMocks from '@models/mocks/workout-mocks';

let [workout1, workout2, workout3, workout4] = workoutMocks.workoutList;

let goalWorkoutList: GoalWorkout[] = [
    { id: 1, isCompleted: false, daysFromStart: 1, workout: workout1 },
    { id: 2, isCompleted: true, daysFromStart: 3, workout: workout2 },
    { id: 3, isCompleted: false, daysFromStart: 5, workout: workout3 },
    { id: 4, isCompleted: true, daysFromStart: 1, workout: workout1 },
    { id: 5, isCompleted: false, daysFromStart: 3, workout: workout2 },
    { id: 6, isCompleted: true, daysFromStart: 5, workout: workout4 },
];
export default goalWorkoutList;