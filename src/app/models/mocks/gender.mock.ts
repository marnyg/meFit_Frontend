import Gender from '@models/gender.model';

const genderList: Gender[] = [
    {
        id: 1,
        name: "Female"
    },
    {
        id: 2,
        name: "Male"
    },
    {
        id: 3,
        name: "Other"
    }
];

export default genderList;