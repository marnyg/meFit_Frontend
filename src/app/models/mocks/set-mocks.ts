import * as exerciseMocks from '@models/mocks/exercise-mocks';
import Set from '@models/set.model';

let [exer1, exer2, exer3, ...rest] = exerciseMocks.exerciseList;
let setList: Set[] = [
    { id: 1, exerciseRepetitions: 1, exercise: exer1 },
    { id: 2, exerciseRepetitions: 2, exercise: exer2 },
    { id: 3, exerciseRepetitions: 10, exercise: exer3 },
    { id: 4, exerciseRepetitions: 5, exercise: exer1 },
    { id: 5, exerciseRepetitions: 22, exercise: exer2 },
    { id: 6, exerciseRepetitions: 99, exercise: exer3 },
    { id: 7, exerciseRepetitions: 123, exercise: exer1 },
    { id: 8, exerciseRepetitions: 4, exercise: exer2 },
    { id: 9, exerciseRepetitions: 5, exercise: exer3 },
    { id: 10, exerciseRepetitions: 1, exercise: exer1 },
];

export default setList;