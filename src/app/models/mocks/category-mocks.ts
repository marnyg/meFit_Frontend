import Category from "@models/category.model";

let cat1: Category = { id: 1, concreteName: "Legs", generalName: "Lower body" };
let cat2: Category = { id: 2, concreteName: "Back" , generalName: "Upper body" };
let cat3: Category = { id: 3, concreteName: "Chest" , generalName: "Upper body" };
let cat4: Category = { id: 7, concreteName: "Abs" , generalName: "Core" };

let categoryList = [cat1, cat2, cat3, cat4];
export default categoryList;