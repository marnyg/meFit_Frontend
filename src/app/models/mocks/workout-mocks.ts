import Workout from "@models/workout.model";
import categoryList from "@models/mocks/category-mocks";
import setList from "@models/mocks/set-mocks";

let [cat1, cat2, cat3, cat4] = categoryList;
let set1 = setList.slice(2, 4);
let set2 = setList.slice(6);
let set3 = setList;
let workoutList: Workout[] = [
    { id: 1, name: "Mock Workout 1", category: [cat1, cat4], sets: set1 },
    { id: 2, name: "Mock Workout 2", category: [cat2, cat4], sets: set2 },
    { id: 3, name: "Mock Workout 3", category: [cat3, cat2], sets: set3 },
    { id: 4, name: "Mock Workout 4", category: [cat1, cat2, cat3, cat4], sets: set1 },
    { id: 5, name: "Mock Workout 5", category: [cat2, cat4], sets: set2 },
    { id: 6, name: "Mock Workout 6", category: [cat1, cat3], sets: set3 },
    { id: 7, name: "Mock Workout 7", category: [cat1, cat2], sets: set2 },
    { id: 8, name: "Mock Workout 8", category: [cat1, cat3], sets: set1 },
    { id: 9, name: "Mock Workout 9", category: [cat1, cat4], sets: set3 },
    { id: 10, name: "Mock Workout 10", category: [cat1, cat4], sets: set2 },
];
let singleWorkout = workoutList[0];
export { workoutList, singleWorkout };