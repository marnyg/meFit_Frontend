import Exercise from './exercise.model';

export default interface Set {
    id: number,
    exerciseRepetitions: number,
    exercise: Exercise;
}