
export default interface Address {
    id: number,
    addressLine1: string,
    addressLine2?: string,
    addressLine3?: string,
    postalCode: number,
    city: string,
    country: string
}