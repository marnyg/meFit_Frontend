import Workout from './workout.model';

export default interface GoalWorkout {
    id: number,
    workout: Workout,
    isCompleted: boolean,
    daysFromStart: number;
}