
export default interface Category{
    id:number,
    concreteName: string,
    generalName: string
}