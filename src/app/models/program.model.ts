import Category from './category.model';
import ProgramWorkout from './program-workout.model';

export default interface Program {
    id: number,
    name: string,
    category: Category[];
    workouts: ProgramWorkout[];
}