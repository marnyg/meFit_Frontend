import Gender from './gender.model';
import Profile from './profile.model';

export default interface User {
    id: number,
    firstName: string,
    lastName: string,
    email: string,
    dob: Date,
    applyForContributor?: boolean,
    role: string,
    profile: Profile,
    gender: Gender,
    isContributor?: boolean,
    isAdmin?: boolean
}