import GoalWorkout from './goal-workout.model';
import Program from './program.model';

export default interface Goal {
    id: number,
    program?: Program,
    goalProgram?: Program,
    goalWorkouts: GoalWorkout[],
    profileId: number,//using id until i hav access to the profile model
    startDate: Date,
    isActive: boolean,
    isCompleted: boolean;
}