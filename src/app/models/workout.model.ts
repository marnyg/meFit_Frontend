import Set from "@models/set.model";
import Category from "@models/category.model";

export default interface Workout {
    id: number,
    name: string,
    description?: string,
    category: Category[],
    sets: Set[];
}