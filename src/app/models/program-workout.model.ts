import Workout from './workout.model';

export default interface ProgramWorkout {
    id: number,
    workoutId?: number,
    workout: Workout,
    workoutName?: string,
    isCompleted: boolean,
    daysFromStart: number;
}