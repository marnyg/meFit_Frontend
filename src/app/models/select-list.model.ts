
export default interface SelectList{
    id:number,
    name: string,
    additionalValue?: number
}