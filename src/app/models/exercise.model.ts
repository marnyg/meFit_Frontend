import Category from './category.model';

export default interface Exercise {
    id: number,
    name: string,
    description: string,
    categories: Category[],
    image: URL,
    video: URL;
}