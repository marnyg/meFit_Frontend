import Address from './address.model';


export default interface Profile {
    id: number,
    weight: number,
    height: number,
    medicalConditions?: string,
    disabilities?: string,
    address: Address
}