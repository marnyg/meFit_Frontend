import { Component, Input, OnInit } from '@angular/core';
import GoalWorkout from '@models/goal-workout.model';
import Goal from '@models/goal.model';
import Workout from '@models/workout.model';
import { GoalsService } from 'src/app/services/goals/goals.service';
import { UserService } from 'src/app/services/user/user.service';
import { WorkoutsService } from 'src/app/services/workouts/workouts.service';

@Component({
  selector: 'app-goals',
  templateUrl: './goals.component.html',
  styleUrls: ['./goals.component.scss']
})
export class GoalsComponent implements OnInit {
  goals: Goal[];
  selectedWorkout: GoalWorkout;
  activeGoal: Goal;
  tabId: number = 0;
  selectedHistoryGoal: Goal;
  selectedHistoryWorkout: GoalWorkout;

  constructor(private goalService: GoalsService, private userService: UserService, private workoutService: WorkoutsService) { }

  async ngOnInit() {
    let user = await this.userService.getUser();
    this.goals = await this.goalService.getGoalsByUserid(user.profile.id);
    this.activeGoal = this.goals.find(g => g.isActive);
    this.activeGoal = await this.goalService.getGoalByGoalId(this.activeGoal.id);
    this.selectedHistoryGoal = await this.goalService.getGoalByGoalId(this.goals[0].id);


    if (this.activeGoal) this.selectedWorkout = this.activeGoal.goalWorkouts[0];
    if (this.activeGoal) this.selectedHistoryWorkout = this.selectedHistoryGoal.goalWorkouts[0];
  }

  onWorkoutChange(workout: GoalWorkout) {
    this.selectedWorkout = workout;
  }
  tabClicked(tabId: number) {
    this.tabId = tabId;
  }
  async onHistoryGoalSelectionChange(goal: Goal) {
    this.selectedHistoryGoal = await this.goalService.getGoalByGoalId(goal.id);

  }
  onHistoryWorkoutChange(workout: GoalWorkout) {
    this.selectedHistoryWorkout = workout;
  }


}
