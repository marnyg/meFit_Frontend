import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder, ValidatorFn, ValidationErrors } from '@angular/forms';
import { Router } from '@angular/router';
import Gender from '@models/gender.model';
import genderList from '@models/mocks/gender.mock';
import { RegisterService } from 'src/app/services/user/register.service';
import { SessionService } from 'src/app/services/user/session.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  genders: Gender[];

  registerForm: FormGroup = this.fb.group({
    firstName: ['', [Validators.required, Validators.minLength(2)]],
    lastName: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(50)]],
    email: ['', [Validators.required, Validators.minLength(2), Validators.email]],
    dob: ['', [Validators.required]],
    password: ['', [Validators.required, Validators.minLength(8)]],
    passwordConfirmation: ['', [Validators.required, Validators.minLength(8)]],
    profile: this.fb.group({
      weight: ['', [Validators.required, Validators.pattern('^\s*[0-9]+([\,\.][0-9][0-9]{0,2})?$|^$|^\s*$')]],
      height: ['', [Validators.required, Validators.pattern('^\s*[0-9]+([\,\.][0-9][0-9]{0,2})?$|^$|^\s*$')]],
      genderId: [''],
      address: this.fb.group({
        addressLine1:['', [Validators.required, Validators.minLength(2)]],
        addressLine2:[''],
        addressLine3:[''],
        country:['', [Validators.required, Validators.minLength(2)]],
        postalCode:['', [Validators.required]],
        city:['', [Validators.required, Validators.minLength(2)]]
      })
    })
  }, {validators: this.passwordMatchValidator})

  constructor(
    private router: Router,
    private session: SessionService,
    private fb: FormBuilder,
    private registerService: RegisterService
  ) {
    if (this.session.get() !== false) {
      this.router.navigateByUrl('/profile');
    }
  }

  get firstName() { return this.registerForm.get('firstName'); }
  get lastName() { return this.registerForm.get('lastName'); }
  get email() { return this.registerForm.get('email'); }
  get dob() { return this.registerForm.get('dob'); }
  get password() { return this.registerForm.get('password'); }

  get profile() { return this.registerForm.get('profile'); }
  get weight() { return this.registerForm.get('profile.weight'); }
  get height() { return this.registerForm.get('profile.height'); }
  get genderId() { return this.registerForm.get('profile.genderId'); }
  
  get address() { return this.registerForm.get('profile.address'); }
  get addressLine1() { return this.registerForm.get('profile.address.addressLine1'); }
  get addressLine2() { return this.registerForm.get('profile.address.addressLine2'); }
  get addressLine3() { return this.registerForm.get('profile.address.addressLine3'); }
  get country() { return this.registerForm.get('profile.address.country'); }
  get postalCode() { return this.registerForm.get('profile.address.postalCode'); }
  get city() { return this.registerForm.get('profile.address.city'); }

  
  ngOnInit(): void {
    this.genders = genderList;
    this.genderId.setValue(this.genders[0].id);
  }

  async onRegisterClicked(): Promise<void> {
    console.log(this.registerForm.value);
    let user = this.registerForm.value;
    user.profile.disabilities = "";
    user.profile.medicalConditions ="";

    try {

      const { role, token } = await this.registerService.register(this.registerForm.value);
      this.session.save(role, token);
      this.router.navigateByUrl('/profile');

    } catch (error) {
      console.log(error);
      //set variable to error and display some message to user here
    }
  }

  passwordMatchValidator(group: FormGroup) {
    return group.get('password').value === group.get('passwordConfirmation').value ? null : { 'mismatch': true };
  }
}
