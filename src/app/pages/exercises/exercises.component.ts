import { Component, OnInit } from '@angular/core';
import { ExercisesService } from 'src/app/services/exercises/exercises.service';
import Exercise from '@models/exercise.model';
import { GoalsService } from 'src/app/services/goals/goals.service';
import Category from '@models/category.model';
import SelectList from '@models/select-list.model';
import { CategoryService } from 'src/app/services/category/category.service';

@Component({
  selector: 'app-exercises',
  templateUrl: './exercises.component.html',
  styleUrls: ['./exercises.component.scss']
})
export class ExercisesComponent implements OnInit {
  exercises: Exercise[];
  exercisesFilteredByCategory: Exercise[];
  selecedExercise: Exercise;
  categories: Category[];
  selectListOfCategories: SelectList[];
  isFilteredForFitness: boolean = false;
  selectedCategoryId: number = -1;

  constructor(
    private exerciseServise: ExercisesService,
    private categoryService: CategoryService
  ) { }

  async ngOnInit(): Promise<void> {
    try {
      this.exercises = await this.exerciseServise.getExercises();
      console.log(this.exercises);
      this.selecedExercise = this.exercises[0];

      const categoriesResult = await this.categoryService.getCategories();
      this.initCategoryFiltering(categoriesResult);
    } catch (error) {
      console.log(error);
    }

  }
  initCategoryFiltering(categoryList: Category[]) {
    this.categories = categoryList;
    this.selectListOfCategories = this.categories.map(category => ({ id: category.id, name: category.concreteName }));
    this.selectListOfCategories.unshift({ id: -1, name: "All" });
    this.exercisesFilteredByCategory = this.exercises;
  }
  onExerciseChange(exercise: Exercise) {
    this.selecedExercise = exercise;
  }

  filterExercises() {
    this.exercisesFilteredByCategory = this.exercises;
    if (this.selectedCategoryId != -1) {
      this.exercisesFilteredByCategory = this.exercises.filter(ex => ex.categories.filter(
        cat => cat.id == this.selectedCategoryId).length > 0);
    }
    if (this.isFilteredForFitness) {
      this.exercisesFilteredByCategory = this.exercisesFilteredByCategory.filter(w => ((w.id % 2) == 0));
    }
  }
}