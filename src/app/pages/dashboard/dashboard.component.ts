import { ViewChild } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { WeeklyOverviewComponent } from '@components/dasboard/weekly-overview/weekly-overview.component';
import Goal from '@models/goal.model';
import { GoalsService } from 'src/app/services/goals/goals.service';
import { UserService } from 'src/app/services/user/user.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  currentGoal: Goal;
  today = new Date();
  constructor(private goalService: GoalsService, private userService: UserService) { }

  async ngOnInit() {
    let user = await this.userService.getUser();
    let goals = await this.goalService.getGoalsByUserid(user.id);
    this.currentGoal = await this.goalService.getGoalByGoalId(goals.find(g => g.isActive).id);
  }
}
