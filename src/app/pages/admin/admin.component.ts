import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user/user.service';
import { userList } from '@models/mocks/user.mock';
import User from '@models/user.model';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  users: User[] = userList;
  selectedUsers: User;

  constructor(private userService: UserService) { }

  async ngOnInit(): Promise<void> {
    try {
      this.users = await this.userService.getUsers();
      this.selectedUsers = this.users[0];
    } catch (error) {
      console.log(error);
    }
  }
  selectProfile(profile: User) {
    this.selectedUsers = profile;
    console.log(this.selectedUsers);
  }

  submitChanges() {
    console.log(this.selectedUsers);
    try {
      this.userService.patchRole(this.selectedUsers);
    } catch (error) {
      console.log(error);
    }
  }

}
