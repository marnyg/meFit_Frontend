import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import Exercise from '@models/exercise.model';
import Workout from '@models/workout.model';
import { WorkoutsService } from 'src/app/services/workouts/workouts.service';
import Category from '@models/category.model';
import SelectList from '@models/select-list.model';
import { CategoryService } from 'src/app/services/category/category.service';

@Component({
  selector: 'app-workouts',
  templateUrl: './workouts.component.html',
  styleUrls: ['./workouts.component.scss']
})
export class WorkoutsComponent implements OnInit {
  @Input() workouts: Workout[];
  @Input() hasSelectButton: boolean = false;
  @Input() headerSubtitle: string = "Here you can see all workouts on our site";
  selectedWorkout: Workout;
  selectedExercise: Exercise;
  @Output() workoutChange = new EventEmitter<Workout>();
  @Output() workoutSelected = new EventEmitter();
  workoutsFilteredByCategory: Workout[];
  categories: Category[];
  selectListOfCategories: SelectList[];
  selectedCategoryId: number = -1;
  isFilteredForFitness: boolean = false;


  constructor(
    private workoutsService: WorkoutsService,
    private categoryService: CategoryService
    ) { }

  async ngOnInit(): Promise<void> {
    try {
      if (!this.workouts) this.workouts = await this.workoutsService.getWorkouts();
      this.selectedWorkout = this.workouts[0];
      console.log(this.workouts);

      const categoriesResult = await this.categoryService.getCategories();
      this.initCategoryFiltering(categoriesResult);
    } catch (error) {
      console.log(error);
    }

  }
  initCategoryFiltering(categoryList: Category[]) {
    this.categories = categoryList;
    this.selectListOfCategories = this.categories.map(category => ({ id: category.id, name: category.concreteName }));
    this.selectListOfCategories.unshift({ id: -1, name: "All" });
    this.workoutsFilteredByCategory = this.workouts;
  }
  changeSelectedWorkout(workout: Workout) {

    this.selectedWorkout = workout;
    this.workoutChange.emit(workout);
  }
  changeSelectedExercise(exercise: Exercise) {
    this.selectedExercise = exercise;
  }
  onWorkoutSelected() {
    this.workoutSelected.emit();
  }

  filterWorkouts() {
    this.workoutsFilteredByCategory = this.workouts;
    if (this.selectedCategoryId != -1) {
      this.workoutsFilteredByCategory = this.workouts.filter(w => w.category.filter(
        cat => cat.id == this.selectedCategoryId).length > 0);
    }
    if (this.isFilteredForFitness) {
      this.workoutsFilteredByCategory = this.workoutsFilteredByCategory.filter(w => ((w.id % 2) == 0));
    }
  }
}
