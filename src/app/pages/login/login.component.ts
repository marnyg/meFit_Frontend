import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginService } from 'src/app/services/user/login.service';
import { SessionService } from 'src/app/services/user/session.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  error: string = '';
  showError: boolean = false;
  showForgotPasswordPopOver: boolean = false;
  resetEmail: string = '';
  loginForm: FormGroup = this.fb.group({
    username: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required, Validators.minLength(6)]]
  })

  constructor(
    private fb: FormBuilder,
    private loginService: LoginService,
    private session: SessionService,
    private router: Router
  ) {
    if (this.session.get() !== false) {
      this.router.navigateByUrl('/profile');
    }
  }

  ngOnInit(): void {

  }
  get username() { return this.loginForm.get('username'); }
  get password() { return this.loginForm.get('password'); }

  async loginClicked(): Promise<void> {

    try {

      const { role, token } = await this.loginService.login(this.loginForm.value);
      this.session.save(role, token);
      this.router.navigateByUrl('/profile');
      this.showError = false;
    } catch (error) {
      console.clear();
      this.error = error;
      this.showError = true;
      setTimeout(() => {
        this.showError = false;
      }, 6000);
      
    }
  }

  closeError(): void{
    this.showError = false;
    this.showForgotPasswordPopOver = false;
  }

  forgotPassword(): void {
    console.log("Forgot password cliked...")
    this.showForgotPasswordPopOver = true;

  }

  async resetPassword(): Promise<void> {
    console.log(this.resetEmail)
    try {
      await this.loginService.resetPassword(this.resetEmail);
    } catch (error) {
      this.error = error;
      this.showError = true;
      setTimeout(() => {
        this.showError = false;
      }, 6000);
    } finally {
      this.showForgotPasswordPopOver = false;
    }
  }
}
