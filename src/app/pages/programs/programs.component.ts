import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import ProgramWorkout from '@models/program-workout.model';
import Program from '@models/program.model';
import { ProgramsService } from 'src/app/services/programs/programs.service';
import Category from '@models/category.model';
import SelectList from '@models/select-list.model';
import { CategoryService } from 'src/app/services/category/category.service';

@Component({
  selector: 'app-programs',
  templateUrl: './programs.component.html',
  styleUrls: ['./programs.component.scss']
})
export class ProgramsComponent implements OnInit {
  @Input() programs: Program[];
  @Input() hasSelectButton: boolean = false;
  @Input() headerSubtitle: string = "Here you can see all programs on our site";
  selectedWorkout: ProgramWorkout;
  selectedProgram: Program;
  @Output() programChange = new EventEmitter<Program>();
  @Output() programSelected = new EventEmitter<Program>();
  categories: Category[];
  selectListOfCategories: SelectList[];
  programsFilteredByCategory: Program[];
  isFilteredForFitness: boolean = false;
  selectedCategoryId: number = -1;

  constructor(
    private programService: ProgramsService,
    private categoryService: CategoryService
    ) { }

  async ngOnInit(): Promise<void> {
    try {
      if (!this.programs) this.programs = await this.programService.getPrograms();
  
      this.selectedProgram = this.programs[0];
      this.programChange.emit(this.selectedProgram);
      
      const categoriesResult = await this.categoryService.getCategories();
      this.initCategoryFiltering(categoriesResult);
      
    } catch (error) {
      console.log(error);
    }
  }
  initCategoryFiltering(categoryList: Category[]) {
    this.categories = categoryList;
    this.selectListOfCategories = this.categories.map(category => ({ id: category.id, name: category.concreteName }));
    this.selectListOfCategories.unshift({ id: -1, name: "All" });
    this.programsFilteredByCategory = this.programs;
  }
  onWorkoutChange(workout: ProgramWorkout) {
    console.log("onEmit", workout);
    this.selectedWorkout = workout;
  }
  onProgramChange(program: Program) {
    this.selectedProgram = program;
    this.programChange.emit(program);
  }
  async onProgramSelected(program: Program) {
    let fullProgram = await this.programService.getProgramById(program.id);
    console.log(fullProgram);

    this.programSelected.emit(fullProgram);
  }
  filterPrograms() {
    this.programsFilteredByCategory = this.programs;
    if (this.selectedCategoryId != -1) {
      this.programsFilteredByCategory = this.programs.filter(p => p.category.filter(
        cat => cat.id == this.selectedCategoryId).length > 0);
    }
    if (this.isFilteredForFitness) {
      this.programsFilteredByCategory = this.programsFilteredByCategory.filter(w => ((w.id % 2) == 0));
    }
  }

}
