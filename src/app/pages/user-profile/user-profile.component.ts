import { Component, OnInit } from '@angular/core';
import User from '@models/user.model';
import aUser from '@models/mocks/user.mock';
import { UserService } from 'src/app/services/user/user.service';
import PutUser from '@models/mocks/putUser.model';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {

  tabId: number = 1;
  user: User;
  profileTest: User;

  constructor(
    private userService: UserService
  ) { }

  async ngOnInit(): Promise<void> {
    try {
      this.user  = await this.userService.getUser();
      console.log(this.user);
    } catch (error) {
      console.log(error);
    }
  }

  async handlePersonalSaveClicked(profileInfo: any): Promise<void> {
    // this.user = Object.assign({},  profileInfo, this.user);
    try {
      
      await this.userService.putUser(this.convertToUpdateObj(profileInfo), this.user.id) ;
      
    } catch (error) {
      console.log(error);
    }
  }

  handleMetricSaveClicked(values: any): void {
    this.user.profile.height = values.height;
    this.user.profile.weight = values.weight;
    console.log(this.user);
  }

  tabClicked(id: number): void {
    console.log(id);
    this.tabId = id;
    // this.userService.getUser();
  }

  convertToUpdateObj(newInfo: any): any {
    let val: PutUser = {...newInfo};
    val.profile.height = this.user.profile.height;
    val.profile.weight = this.user.profile.weight;
    val.profile.disabilities = this.user.profile.disabilities;
    val.profile.medicalConditions = this.user.profile.medicalConditions;
    // val.profile.address.postalCode = val.profile.address.postalCode.toString();
    console.log(val);
    return val;
  }
}
