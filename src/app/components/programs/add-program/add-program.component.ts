import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Category from '@models/category.model';
import SelectList from '@models/select-list.model';
import Workout from '@models/workout.model';
import { CategoryService } from 'src/app/services/category/category.service';
import { ProgramsService } from 'src/app/services/programs/programs.service';
import { WorkoutsService } from 'src/app/services/workouts/workouts.service';

@Component({
  selector: 'app-add-program',
  templateUrl: './add-program.component.html',
  styleUrls: ['./add-program.component.scss']
})
export class AddProgramComponent implements OnInit, OnDestroy {
  @Output() clicked = new EventEmitter<boolean>();
  form: FormGroup;
  categories: Category[];
  selectListOfCategories: SelectList[];
  existingWorkouts: Workout[];
  selectListOfExercises: SelectList[];

  constructor(
    fb: FormBuilder,
    private programsService: ProgramsService,
    private categoryService: CategoryService,
    private workoutService: WorkoutsService
  ) {
    this.form = fb.group({
      name: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
      description: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(100)]],
      categoryIds: ['', [Validators.required]],
      programWorkouts: [[]]
    })
  }

  async ngOnInit(): Promise<void> {
    try {
      this.categories = await this.categoryService.getCategories();
      this.existingWorkouts = await this.workoutService.getWorkouts();
    } catch (error) {

    }

    this.selectListOfCategories = this.categories.map(category => ({ id: category.id, name: category.generalName }));
    this.selectListOfExercises = this.existingWorkouts.map(workout => ({ id: workout.id, name: workout.name }));
  }

  get name() { return this.form.get('name') }
  get description() { return this.form.get('description') }
  get categoryIds() { return this.form.get('categoryIds') }
  get programWorkouts() { return this.form.get('programWorkouts') }

  async save() {
    try {
      let newProgram = JSON.parse(JSON.stringify(this.form.value));
      delete newProgram.description;
      await this.programsService.postProgram(newProgram);
    } catch (error) {
      console.log(error);
    }

    this.clicked.emit();
    this.form.reset();
  }

  cancel() {
    this.clicked.emit();
    this.form.reset();
  }

  updateCategories(categories: SelectList[]) {
    this.categoryIds.setValue(categories.map(category => category.id));
  }

  updateExercises(workouts: SelectList[]) {
    this.programWorkouts.setValue(workouts.map(workout => ({
      workoutId: workout.id,
      daysFromStart: workout.additionalValue
    })));
  }

  ngOnDestroy(): void {
    this.form.reset();
  }
}
