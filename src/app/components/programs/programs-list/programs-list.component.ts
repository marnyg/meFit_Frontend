import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import Program from '@models/program.model';
import { SessionService } from 'src/app/services/user/session.service';

@Component({
  selector: 'app-programs-list',
  templateUrl: './programs-list.component.html',
  styleUrls: ['./programs-list.component.scss']
})
export class ProgramsListComponent implements OnInit {
  @Input() programs: Program[];
  @Input() selectedProgram: Program;
  @Output() progamSelectionChange = new EventEmitter<Program>();
  @Output() progamSelected = new EventEmitter<Program>();
  @Input() hasSelectButton: boolean = false;
  toggleProgramPopOver: boolean = false;

  constructor(private session: SessionService) { }

  ngOnInit(): void {
    console.log(this.programs);

  }
  onProgramChange(program: Program) {
    this.progamSelectionChange.emit(program);
  }
  programSelected() {
    this.progamSelected.emit(this.selectedProgram);
  }

  toggleAddProgramModule() {
    this.toggleProgramPopOver = true;
  }

  handleSaveProgram() {
    this.toggleProgramPopOver = false;
  }

  isAuthorized(): boolean {
    return this.session.getAdmin() || this.session.getContributor();
  }
}
