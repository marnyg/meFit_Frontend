import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import ProgramWorkout from '@models/program-workout.model';
import Workout from '@models/workout.model';
import { WorkoutsService } from 'src/app/services/workouts/workouts.service';

@Component({
  selector: 'app-program-workout-list',
  templateUrl: './program-workout-list.component.html',
  styleUrls: ['./program-workout-list.component.scss']
})
export class ProgramWorkoutListComponent implements OnInit {
  @Input() workouts: ProgramWorkout[];
  @Input() selectedWorkout: ProgramWorkout;
  work: Workout;
  selectedWorkoutId: number = 0;
  @Output() workoutSelectionChange = new EventEmitter<ProgramWorkout>();
  showCard: boolean = false;

  constructor(private workoutService: WorkoutsService) { }

  async ngOnInit(): Promise<void> {
    // if (this.selectedWorkoutId !== -1)
    // this.work = await this.workoutService.getWorkoutById(this.selectedWorkoutId);
  }

  async ngOnChanges() {
    if (this.selectedWorkout) {
      this.selectedWorkoutId = this.selectedWorkout.workoutId;
    }
    else
      this.selectedWorkoutId = -1;
  }

  async changeSelectedWorkout(selctedWorkout: ProgramWorkout) {
    if (selctedWorkout.workoutId == this.selectedWorkoutId) {
      this.workoutSelectionChange.emit();
    } else {
      this.workoutSelectionChange.emit(selctedWorkout);
    }

  }

  toggleCard(workout) {
  }

}
