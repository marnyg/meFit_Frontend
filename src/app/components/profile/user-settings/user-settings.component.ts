import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from 'src/app/services/user/user.service';


@Component({
  selector: 'app-user-settings',
  templateUrl: './user-settings.component.html',
  styleUrls: ['./user-settings.component.scss']
})
export class UserSettingsComponent implements OnInit {

  @Input() id: number;
  passwordChangeSuccess: boolean = false;
  @Input() applyForContributor: boolean = false;

  form: FormGroup = this.fb.group({
    password: ['', [Validators.required]],
    newPassword: ['', [Validators.required, Validators.minLength(8)]],
    newPasswordConfirm: ['', [Validators.required, Validators.minLength(8)]]
  }, { validators: this.passwordMatchValidator });

  constructor(
    private fb: FormBuilder,
    private userService: UserService
  ) { }

  ngOnInit(): void {
  }

  get password() { return this.form.get('password'); }
  get newPassword() { return this.form.get('newPassword'); }

  async onSavePasswordClicked(): Promise<void> {
    console.log(`Saving password... ${this.newPassword.value}`);
    try {
      
      await this.userService.changePassword(this.id, this.newPassword.value, this.password.value);
      this.passwordChangeSuccess = true;
    } catch (error) {
      console.log(error);
      this.passwordChangeSuccess = false;
    }
    this.form.reset();
    setTimeout(() => {
      this.passwordChangeSuccess = false;
    }, 5000);
  }

  async onApplyContributorClicked(): Promise<void> {
    if(this.applyForContributor) return;
    const apply = [{
      "value": "true", 
      "path": "/applyForContributor", 
      "op": "replace"
    }];
    try {
      await this.userService.patchUser(apply, this.id);
      this.applyForContributor = true;
    } catch (error) {
      console.log(error);
    }
    
    // button should not be visable if user has already applied. get this data from api and do a ng-if in html
  }

  passwordMatchValidator(group: FormGroup) {
    return group.get('newPassword').value === group.get('newPasswordConfirm').value ? null : { 'mismatch': true };
  }

}
