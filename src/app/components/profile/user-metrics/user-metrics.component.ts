import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from 'src/app/services/user/user.service';

@Component({
  selector: 'app-user-metrics',
  templateUrl: './user-metrics.component.html',
  styleUrls: ['./user-metrics.component.scss']
})
export class UserMetricsComponent implements OnInit {

  @Input() weightInput: number;
  @Input() heightInput: number;
  @Input() id: number;
  successfullUpdate: boolean = false;
  @Output() saveClicked: EventEmitter<any> = new EventEmitter();

  metricsForm: FormGroup = this.fb.group({
    weight: ['', [Validators.required, Validators.pattern(/^\d*\.?\d+$/)]],
    height: ['', [Validators.required, Validators.pattern(/^\d*\.?\d+$/)]]
  })

  constructor(
    private fb: FormBuilder,
    private userService: UserService
  ) { }

  get weight() { return this.metricsForm.get('weight'); }
  get height() { return this.metricsForm.get('height'); }

  ngOnInit(): void {
    this.metricsForm.patchValue({ weight: this.weightInput, height: this.heightInput });
  }

  async onSaveClicked() {
    this.saveClicked.emit(this.metricsForm.value);
    
    const values = this.patchList(this.metricsForm);
    
    if(values.length === 0) return;
    try {
      await this.userService.patchUser(values, this.id); 
      this.successfullUpdate = true;
    } catch (error) {
      console.log(error);
    } finally {
      setTimeout(() => {
        this.successfullUpdate = false;
      }, 5000);
    }
  }

  patchList(form: any) {
    let dirtyValues = [];

    Object.keys(form.controls)
      .forEach(key => {
        const currentControl = form.controls[key];

        if (currentControl.dirty) {
          dirtyValues.push({
            "value": currentControl.value,
            "path": `/profile/${key}`,
            "op": "replace"
          })
        }
      });

    return dirtyValues;
  }

}
