import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DatePipe } from '@angular/common';

import Gender from '@models/gender.model';
import { ProfileService } from 'src/app/services/profile/profile.service';
import { Subscription } from 'rxjs';
import User from '@models/user.model';

@Component({
  selector: 'app-personal-information',
  templateUrl: './personal-information.component.html',
  styleUrls: ['./personal-information.component.scss']
})
export class PersonalInformationComponent implements OnInit {

  @Input() userData: User;
  @Output() saveClicked: EventEmitter<User> = new EventEmitter();

  genders: Gender[] = [];
  private formSubscrition: Subscription;

  personalForm: FormGroup = this.fb.group({
    firstName: ['', [Validators.required, Validators.minLength(2), Validators.pattern('^[a-zæøåA-ZÆØÅ -]*$')]],
    lastName: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(50), Validators.pattern('^[a-zæøåA-ZÆØÅ -]*$')]],
    email: [{ value: '', disabled: true }, [Validators.required, Validators.minLength(2), Validators.email]],
    dob: ['', [Validators.required]],
    profile: this.fb.group({
      address: this.fb.group({
        addressLine1: ['', [Validators.required, Validators.minLength(2)]],
        country: ['', [Validators.required, Validators.minLength(2), Validators.pattern('^[a-zæøåA-ZÆØÅ ]*$')]],
        postalCode: ['', [Validators.required, Validators.pattern('^[0-9]*$')]],
        city: ['', [Validators.required, Validators.minLength(2), Validators.pattern('^[a-zæøåA-ZÆØÅ ]*$')]]
      }),
      genderId: ['']
    })
  });

  constructor(
    private datePipe: DatePipe,
    private fb: FormBuilder,
    private profileService: ProfileService
  ) { }

  get profile() { return this.personalForm.get('profile'); }
  get address() { return this.personalForm.get('profile.address'); }
  get genderId() { return this.personalForm.get('profile.genderId'); }
  get firstName() { return this.personalForm.get('firstName'); }
  get lastName() { return this.personalForm.get('lastName'); }
  get email() { return this.personalForm.get('email'); }
  get dob() { return this.personalForm.get('dob'); }
  get addressLine1() { return this.personalForm.get('profile.address.addressLine1'); }
  get country() { return this.personalForm.get('profile.address.country'); }
  get postalCode() { return this.personalForm.get('profile.address.postalCode'); }
  get city() { return this.personalForm.get('profile.address.city'); }


  async ngOnInit(): Promise<void> {
    this.personalForm.patchValue(this.userData);
    this.dob.setValue(this.datePipe.transform(this.dob.value, 'yyyy-MM-dd'));
    this.genderId.setValue(this.userData.gender.id);
    try {
      this.genders = await this.profileService.getGendersList();
    } catch (error) {
      console.log("ERROR: " + error);
    }
  }

  onSaveClicked(): void {
    this.saveClicked.emit(this.personalForm.value);
  }

  /* 
    getDate(): string {
      return (`${this.DOB.value.getFullYear()}-${this.DOB.value.getMonth()+1}-${this.DOB.value.getDate()}`);
    }
   */


}
