import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import Exercise from '@models/exercise.model';
import { SessionService } from 'src/app/services/user/session.service';

@Component({
  selector: 'app-exercises-list',
  templateUrl: './exercises-list.component.html',
  styleUrls: ['./exercises-list.component.scss']
})
export class ExercisesListComponent implements OnInit {
  @Input() exerciseList: Exercise[];
  @Input() selectedExercise: Exercise;
  @Output() exerciseChanged = new EventEmitter<Exercise>();
  toggleExercisePopOver: boolean = false;

  constructor(
    private session: SessionService
  ) { 

  }
  ngOnInit(): void {
  }

  changeSelectedExercise(selctedExercise: Exercise) {
    this.exerciseChanged.emit(selctedExercise);
  }

  toggleAddExerciseModule(){
    this.toggleExercisePopOver = true;
  }

  handleSaveExercise(){
    this.toggleExercisePopOver = false;
  }

  isAuthorized(): boolean {
    return this.session.getAdmin() ||  this.session.getContributor();
  }
}
