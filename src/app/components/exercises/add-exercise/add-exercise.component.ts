import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Category from '@models/category.model';
import Exercise from '@models/exercise.model';
import SelectList from '@models/select-list.model';
import { CategoryService } from 'src/app/services/category/category.service';
import { ExercisesService } from 'src/app/services/exercises/exercises.service';

@Component({
  selector: 'app-add-exercise',
  templateUrl: './add-exercise.component.html',
  styleUrls: ['./add-exercise.component.scss']
})
export class AddExerciseComponent implements OnInit, OnDestroy {
  @Output() clicked = new EventEmitter<boolean>();
  form: FormGroup;
  categories: Category[];
  selectListOfCategories: SelectList[];
  private newExercise = {} as Exercise;

  constructor(
    fb: FormBuilder,
    private exerciseService: ExercisesService,
    private categoryService: CategoryService
    ) {
    this.form = fb.group({
      name: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
      description: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(100)]],
      targetMuscleGroups: ['', [Validators.required]],
      image: ['', [Validators.maxLength(500), Validators.pattern(/^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/)]],
      video: ['', [Validators.maxLength(500), Validators.pattern(/^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/)]]
    });
  }
  
  async ngOnInit(): Promise<void> {
    try {
      this.categories = await this.categoryService.getCategories(); 
    } catch (error) {
      console.log(error);
    }
    this.selectListOfCategories = this.categories.map(category => ({ id: category.id, name: category.concreteName }));
  }

  get name() { return this.form.get('name'); }
  get description() { return this.form.get('description'); }
  get targetMuscleGroups() { return this.form.get('targetMuscleGroups'); }
  get image() { return this.form.get('image'); }
  get video() { return this.form.get('video'); }

  async save() {
    this.newExercise = JSON.parse(JSON.stringify(this.form.value));
    console.log(this.newExercise);
    try {
      await this.exerciseService.postExercise(this.newExercise);
    } catch (error) {
      console.log(error);
    }
    this.clicked.emit();
    this.form.reset();
  }

  cancel() {
    this.clicked.emit();
    this.form.reset();
  }

  updateCategories(categories: SelectList[]) {
    this.targetMuscleGroups.setValue(categories.map(category => category.id));
  }

  ngOnDestroy(): void {
    this.form.reset();
  }
}
