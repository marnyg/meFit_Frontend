import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import GoalWorkout from '@models/goal-workout.model';
import Goal from '@models/goal.model';
import programList from '@models/mocks/program-mocks';
import ProgramWorkout from '@models/program-workout.model';
import Program from '@models/program.model';
import Workout from '@models/workout.model';
import { GoalsComponent } from 'src/app/pages/goals/goals.component';
import { GoalsService } from 'src/app/services/goals/goals.service';
import { ProgramsService } from 'src/app/services/programs/programs.service';
import { WorkoutsService } from 'src/app/services/workouts/workouts.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-create-goal',
  templateUrl: './create-goal.component.html',
  styleUrls: ['./create-goal.component.scss']
})
export class CreateGoalComponent implements OnInit {
  TMPcounterFORMOCKID = 9;//TO BE REMOVED
  programs: Program[];
  workouts: Workout[];
  filteredProgramList: Program[];
  filteredWorkoutsList: Workout[];
  selectedProgram: Program;
  selectedWorkout: Workout;

  goalToDifficult: boolean = false;
  tabId: number = 0;
  @Input() newGoal: Goal;
  @Input() headingString: string = "New Goal";

  constructor(private route: ActivatedRoute, private programService: ProgramsService, private workoutService: WorkoutsService) { }

  async ngOnInit(): Promise<void> {
    if (!this.newGoal)
      this.newGoal = { program: null, goalWorkouts: [], profileId: -1, startDate: new Date(), id: -1, isActive: true, isCompleted: false };
    this.programs = await this.programService.getPrograms();
    this.workouts = await this.workoutService.getWorkouts();
  }
  checkIfGoalToDificult() {
    let numWorkouts = this.newGoal.goalWorkouts.length + this.newGoal.program?.workouts.length;
    this.goalToDifficult = numWorkouts >= 10;

  }

  onProgramChange(program: Program) {
    this.selectedProgram = program;
  }
  onWorkoutChange(workout: Workout) {
    this.selectedWorkout = workout;
  }
  addProgToGoal() {
    console.log(this.newGoal);
    this.newGoal.program = this.selectedProgram;
    this.newGoal.goalProgram = this.selectedProgram;
    this.checkIfGoalToDificult();
  }
  addWorkoutToGoal() {
    let newGoalWorkout: GoalWorkout = { id: this.TMPcounterFORMOCKID, isCompleted: false, daysFromStart: 3, workout: this.selectedWorkout };
    this.TMPcounterFORMOCKID++;
    this.newGoal.goalWorkouts.push(newGoalWorkout);
    this.checkIfGoalToDificult();
  }
  tabClicked(num: number) {
    this.tabId = num;
  }
  removeWorkout(workout: GoalWorkout) {
    this.newGoal.goalWorkouts = this.newGoal.goalWorkouts.filter(pw => pw.id !== workout.id);
    this.checkIfGoalToDificult();
  }
  removeProgram() {
    this.newGoal.program = null;
    this.checkIfGoalToDificult();
  }

}
