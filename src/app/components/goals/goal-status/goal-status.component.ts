import { Component, Input, OnInit } from '@angular/core';
import Program from '@models/program.model';

@Component({
  selector: 'app-goal-status',
  templateUrl: './goal-status.component.html',
  styleUrls: ['./goal-status.component.scss']
})
export class GoalStatusComponent implements OnInit {
  @Input() program: Program;

  constructor() { }

  ngOnInit(): void {
  }

}
