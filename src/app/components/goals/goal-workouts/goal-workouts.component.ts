import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import GoalWorkout from '@models/goal-workout.model';
import Goal from '@models/goal.model';
import ProgramWorkout from '@models/program-workout.model';
import Program from '@models/program.model';
import Workout from '@models/workout.model';
import { WorkoutsService } from 'src/app/services/workouts/workouts.service';

@Component({
  selector: 'app-goal-workouts',
  templateUrl: './goal-workouts.component.html',
  styleUrls: ['./goal-workouts.component.scss']
})
export class GoalWorkoutsComponent implements OnInit {
  @Input() goalWorkouts: GoalWorkout[];
  @Input() goal: Goal;
  @Input() goalProgram: Program;
  @Input() isInteractiv: boolean = true;
  @Input() isDeletable: boolean = false;
  @Input() isCompletable: boolean = true;
  @Output() workoutChanged = new EventEmitter<GoalWorkout>();
  @Output() deleteWorkout = new EventEmitter<GoalWorkout>();
  @Output() deleteProgam = new EventEmitter<Program>();
  selectedWorkout: Workout;


  constructor(private workoutsService: WorkoutsService) { }

  ngOnInit(): void {
    if (this.goalWorkouts && this.goalWorkouts.length > 0) this.selectedWorkout = this.goalWorkouts[0]?.workout;
    if (this.goalProgram) this.selectedWorkout = this.goalProgram.workouts[0]?.workout;
  }
  ngOnChanges(): void {
    if (this.goalWorkouts && this.goalWorkouts.length > 0) this.selectedWorkout = this.goalWorkouts[0].workout;
    if (this.goalProgram) this.selectedWorkout = this.goalProgram.workouts[0].workout;
    if (!this.selectedWorkout && this.goal.goalWorkouts && this.goal.goalWorkouts[0]) this.selectedWorkout = this.goal.goalWorkouts[0].workout;
  }
  async onWorkoutSelectionChange(workout: GoalWorkout) {
    this.selectedWorkout = await this.workoutsService.getWorkoutById(workout.workout.id);
    this.workoutChanged.emit(workout);
  }
  onDeleteWorkout(workout: GoalWorkout) {
    this.deleteWorkout.emit(workout);
  }
  onDeleteProgram(program: Program) {
    this.deleteProgam.emit(program);
  }
  getProgramWorkoutDate(workout: ProgramWorkout): Date {
    let date = new Date();
    date.setDate(this.goal.startDate.getDate() + workout.daysFromStart);
    return date;
  }
  getGoalWorkoutDate(workout: GoalWorkout) {
  }
  setGoalStartDate(event) {
    this.goal.startDate = new Date(event);
  }
}
