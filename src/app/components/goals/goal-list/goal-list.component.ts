import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import Goal from '@models/goal.model';
import { GoalsService } from 'src/app/services/goals/goals.service';

@Component({
  selector: 'app-goal-list',
  templateUrl: './goal-list.component.html',
  styleUrls: ['./goal-list.component.scss']
})
export class GoalListComponent implements OnInit {
  @Input() goals: Goal[];
  @Output() goalChange = new EventEmitter<Goal>();
  selectedGoal: Goal;

  constructor(private goalService: GoalsService) { }

  ngOnInit() {
    // this.selectedGoal  = await this.goalService.getGoalByGoalId(this.goals[0].id);
    this.selectedGoal = this.goals[0];
    this.goalChange.emit(this.selectedGoal);
  }
  ngOnChange() {
  }

  onGoalChange(goal: Goal) {
    this.selectedGoal = goal;
    this.goalChange.emit(goal);
  }

}
