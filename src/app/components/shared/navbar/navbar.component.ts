import { Component, OnInit } from '@angular/core';
import { SessionService } from 'src/app/services/user/session.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  constructor(
    private session: SessionService
  ) { }

  ngOnInit(): void {
  }

  toggleHamburger() {
    let navbar = document.getElementById("navbarBasicExample");
    
    if (navbar.classList.contains("is-active")) 
      navbar.classList.remove("is-active");
    else navbar.classList.add("is-active");
  }

  isSignedIn(): boolean{
    return this.session.get() !== false;
  }

  isAdmin(): boolean {
    return this.session.getAdmin();
  }

  logOut(): void {
    this.session.logOut();
  }

}
