import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import SelectList from '@models/select-list.model';

@Component({
  selector: 'app-select-list',
  templateUrl: './select-list.component.html',
  styleUrls: ['./select-list.component.scss']
})
export class SelectListComponent implements OnInit, OnDestroy {

  @Input() list: SelectList[];
  @Input() toggleAdditionalInput: boolean = false;
  @Input() additionalInputHeadline: string = '';
  @Input() additionalInputPattern: string;
  @Output() updateSelectedItems = new EventEmitter<SelectList[]>();
  choosenList: SelectList[] = [];
  form: FormGroup;

  constructor(
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.form = this.fb.group({
      name: [''],
      selectedNameForDelete: [''],
      additionalValue: ['', [Validators.required, Validators.pattern(this.additionalInputPattern)]]
    })
    this.name.setValue(this.list[0].id);
  }

  get name() { return this.form.get('name') }
  get selectedNameForDelete() { return this.form.get('selectedNameForDelete') }
  get additionalValue() { return this.form.get('additionalValue') }


  addItem() {
    // JSON.parse(JSON.stringify(this.form.value))
    if (this.choosenList.find(c => this.name.value == c.id) && !this.toggleAdditionalInput) return;
    this.choosenList.push(JSON.parse(JSON.stringify(this.list.find(c => c.id == this.name.value))));
    this.choosenList[this.choosenList.length - 1].additionalValue = this.additionalValue.value;
    console.log(this.choosenList);
    this.updateSelectedItems.emit(this.choosenList);
  }
  removeItem() {
    console.log(`INDEX? ${this.selectedNameForDelete.value}`);
    this.choosenList.splice(this.selectedNameForDelete.value, 1);
    // this.choosenList = this.choosenList.filter(c => c.id !== this.selectedNameForDelete.value[0]);
    this.updateSelectedItems.emit(this.choosenList);
  }

  ngOnDestroy() {
    this.form.reset();
  }
}
