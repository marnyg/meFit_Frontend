import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-error-popup',
  templateUrl: './error-popup.component.html',
  styleUrls: ['./error-popup.component.scss']
})
export class ErrorPopupComponent implements OnInit {

  @Input() errorMessage: string = '';
  @Input() additionalInformation: string = '';
  @Output() closeModal: EventEmitter<void> = new EventEmitter();
  constructor() { }

  ngOnInit(): void {
  }

  closeMe(): void {
    this.closeModal.emit();
  }

}
