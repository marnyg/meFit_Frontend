import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import Exercise from '@models/exercise.model';
import Set from '@models/set.model';
import { workoutList } from '@models/mocks/workout-mocks';
import Workout from '@models/workout.model';
import { WorkoutsService } from 'src/app/services/workouts/workouts.service';

@Component({
  selector: 'app-workout-details',
  templateUrl: './workout-details.component.html',
  styleUrls: ['./workout-details.component.scss']
})
export class WorkoutDetailsComponent implements OnInit {
  @Input() workout: Workout;
  @Input() workoutID: number;
  selectedExercise: Set;

  constructor(private workoutService: WorkoutsService) { }

  async ngOnInit(): Promise<void> {
    console.log(this.workout, this.workoutID);

    if (!this.workout)
      this.workout = await this.workoutService.getWorkoutById(this.workoutID);
    this.selectedExercise = this.workout.sets[0];
    console.log(this.workout, this.workoutID);
  }

  async ngOnChanges() {
    try {
      this.workout = await this.workoutService.getWorkoutById(this.workoutID);

    } catch (error) {
      console.log(error);
    }

  }

  changeSelectedExercise(exercise: Set) {
    this.selectedExercise = exercise;
  }
}
