import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Category from '@models/category.model';
import Exercise from '@models/exercise.model';
import SelectList from '@models/select-list.model';
import { CategoryService } from 'src/app/services/category/category.service';
import { ExercisesService } from 'src/app/services/exercises/exercises.service';
import { WorkoutsService } from 'src/app/services/workouts/workouts.service';

@Component({
  selector: 'app-add-workout',
  templateUrl: './add-workout.component.html',
  styleUrls: ['./add-workout.component.scss']
})
export class AddWorkoutComponent implements OnInit, OnDestroy {

  @Output() clicked = new EventEmitter<boolean>();
  form: FormGroup;
  categories: Category[];
  selectListOfCategories: SelectList[];
  exercises: Exercise[];
  selectListOfExercises: SelectList[];

  constructor(
    fb: FormBuilder,
    private workoutService: WorkoutsService,
    private exerciseService: ExercisesService,
    private categoryService: CategoryService
    ) {
    this.form = fb.group({
      name: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
      description: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(100)]],
      categoryIds: ['', [Validators.required]],
      sets: [[]]
    })
  }

  async ngOnInit(): Promise<void> {
    try {
      this.categories = await this.categoryService.getCategories(); 
      this.exercises = await this.exerciseService.getExercises();
      console.log(this.exercises);
    } catch (error) {
      console.log(error);
    }
    this.selectListOfCategories = this.categories.map(category => ({ id: category.id, name: category.concreteName }));
    this.selectListOfExercises = this.exercises.map(exercise => ({ id: exercise.id, name: exercise.name }));
  }

  get name() { return this.form.get('name') }
  get description() { return this.form.get('description') }
  get categoryIds() { return this.form.get('categoryIds') }
  get sets() { return this.form.get('sets') }

  async save() {
    try {
      let newWorkout = JSON.parse(JSON.stringify(this.form.value));
      delete newWorkout.description;
      console.log(newWorkout)
      this.workoutService.postWorkout(newWorkout);
    } catch (error) {
      console.log(error);
    }
    this.clicked.emit();
    this.form.reset();
  }

  cancel() {
    this.clicked.emit();
    this.form.reset();
  }

  updateCategories(categories: SelectList[]) {
    this.categoryIds.setValue(categories.map(category => category.id));
  }

  updateExercises(exercises: SelectList[]) {
    this.sets.setValue(exercises.map(exercise => ({
      exerciseId: exercise.id,
      exerciseRepetitions: exercise.additionalValue
    })));
  }

  ngOnDestroy(): void {
    this.form.reset();
  }
}
