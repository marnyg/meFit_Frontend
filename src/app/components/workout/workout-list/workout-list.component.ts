import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import Workout from '@models/workout.model';
import { SessionService } from 'src/app/services/user/session.service';

@Component({
  selector: 'app-workout-list',
  templateUrl: './workout-list.component.html',
  styleUrls: ['./workout-list.component.scss']
})
export class WorkoutListComponent implements OnInit {
  @Input() workouts: Workout[];
  @Input() selectedWorkout: Workout;
  @Output() workoutSelectionChange = new EventEmitter<Workout>();
  @Output() workoutSelected = new EventEmitter();
  @Input() hasSelectButton: boolean = false;
  toggleWorkoutPopOver: boolean = false;

  constructor(
    private session: SessionService
  ) { }


  ngOnInit(): void {
    this.selectedWorkout = this.workouts[0];
    this.workoutSelectionChange.emit(this.selectedWorkout);
  }

  changeSelectedWorkout(selctedWorkout: Workout) {
    this.workoutSelectionChange.emit(selctedWorkout);
  }
  onWorkoutSelected() {
    this.workoutSelected.emit();
  }

  toggleAddWorkoutModule() {
    this.toggleWorkoutPopOver = true;
  }

  handleSaveExercise() {
    this.toggleWorkoutPopOver = false;
  }

  isAuthorized(): boolean {
    return this.session.getAdmin() ||  this.session.getContributor();
  }
}
