import { Component, Input, OnInit } from '@angular/core';
import GoalWorkout from '@models/goal-workout.model';
import Goal from '@models/goal.model';
import ProgramWorkout from '@models/program-workout.model';
import Workout from '@models/workout.model';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { WorkoutsService } from 'src/app/services/workouts/workouts.service';

@Component({
  selector: 'app-weekly-overview',
  templateUrl: './weekly-overview.component.html',
  styleUrls: ['./weekly-overview.component.scss']
})
export class WeeklyOverviewComponent implements OnInit {
  today = new Date();
  weekdays: string[] = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday",];
  @Input() currentGoal: Goal;
  workoutLists = [];
  daysLeft: number = 0;
  workoutsLeft: number = 0;
  workoutsSkipped: number = 0;

  constructor(private workoutService: WorkoutsService) { }

  getWorkoutsGroupedByDay() {
    this.workoutLists = [];
    [...Array(7).keys()].forEach(num => { this.workoutLists.push(this.getWorkoutsForDay(num)); });
  }
  async ngOnInit(): Promise<void> {
    this.getWorkoutsGroupedByDay();
    this.daysLeft = this.currentGoal.startDate.getDay() + 7 - this.today.getDay();
    this.workoutsLeft = this.getNumWorkoutsLeft();
    this.workoutsSkipped = this.getNumIncompleteWorkouts();
  };

  async ngDoCheck() {
    this.getWorkoutsGroupedByDay();
    this.workoutsLeft = this.getNumWorkoutsLeft();
    this.workoutsSkipped = this.getNumIncompleteWorkouts();
  }
  async ngOnChanges() {
    this.getWorkoutsGroupedByDay();
    this.workoutsLeft = this.getNumWorkoutsLeft();
    this.workoutsSkipped = this.getNumIncompleteWorkouts();
  }

  onDrop(event) {
    let newDay = Number.parseInt(event.container.id.split('-')[1]);
    event.item.data.daysFromStart = newDay;

    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data,
        event.previousIndex,
        event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex, event.currentIndex);
    }
  }

  getDateOffsetFromGoalStart(offset: number): Date {
    let date = new Date();
    date.setDate(this.currentGoal.startDate.getDate() + offset);
    return date;
  }
  shiftStartOfWeekToGoalStart(offset: number): Date {
    let date = new Date();
    date.setDate(this.currentGoal.startDate.getDate() + offset);
    return date;
  }
  getTodayShiftedIndex() {
    return (this.today.getDate() - this.currentGoal.startDate.getDate());
  }

  getWorkoutsForDay(day: number) {
    let workouts: Array<GoalWorkout | ProgramWorkout> = [];
    if (this.currentGoal) {
      workouts = workouts.concat(this.currentGoal.goalWorkouts.filter(gw => gw.daysFromStart === day));
      if (this.currentGoal.program)
        workouts = workouts.concat(this.currentGoal.program.workouts.filter(pw => pw.daysFromStart === day));
    }
    return workouts;
  }
  getNumIncompleteWorkouts() {
    let num: number = 0;
    if (this.currentGoal) {
      num += this.currentGoal.goalWorkouts.filter(gw => gw.daysFromStart < this.getTodayShiftedIndex() && !gw.isCompleted).length;
      if (this.currentGoal.program)
        num += this.currentGoal.program.workouts.filter(pw => pw.daysFromStart < this.getTodayShiftedIndex() && !pw.isCompleted).length;
    }
    return num;
  }
  getNumWorkoutsLeft() {
    let num: number = 0;
    if (this.currentGoal) {
      num += this.currentGoal.goalWorkouts.filter(gw => gw.daysFromStart >= this.getTodayShiftedIndex() && !gw.isCompleted).length;
      if (this.currentGoal.program)
        num += this.currentGoal.program.workouts.filter(pw => pw.daysFromStart >= this.getTodayShiftedIndex() && !pw.isCompleted).length;
    }
    return num;
  }
}

