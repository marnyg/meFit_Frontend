import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { SessionService } from 'src/app/services/user/session.service';

@Injectable({
  providedIn: 'root'
})
export class AccessGuard implements CanActivate {

  constructor(
    private session: SessionService,
    private router: Router
  ) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    if (this.session.getAdmin() !== false || (this.session.getContributor() !== false && this.session.getAdmin() !== false) ) {
      this.router.navigateByUrl(`/${route.url[0].path}/admin`)
      return true;
    }
    if(this.session.getContributor() !== false){
      this.router.navigateByUrl(`/${route.url[0].path}/contributor`)
      return true;
    }else {
      return false;
    }

  }

}
