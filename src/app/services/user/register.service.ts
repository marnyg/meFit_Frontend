import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor(
    private http: HttpClient
  ) { }

  register(user: any): Promise<any> {
    return this.http.post(`${environment.apiUrl}/api/user/register`, 
      user  
    ).pipe(

      map((response: any) => {
        console.log(response);
/*         if (response.status >= 400) {
          return throwError(response.error);
        } */

        return { role: response.role, token: response.token }

      }),

      catchError(errorResponse => {
        const { error } = errorResponse.error || errorResponse.message;
        return throwError(error);
      })

    ).toPromise();
  }

}
