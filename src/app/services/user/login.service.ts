import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { map, catchError, tap } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(
    private http: HttpClient
  ) { }

  login( user: any ): Promise<any> {
    return this.http.post(`${environment.apiUrl}/api/user/login`, {
      "email": user.username,
      "password": user.password
    }).pipe(

      map((response: any) => {
        console.log(response);

        return { role: response.role, token: response.token }

      }),

      catchError(errorResponse => {
        const { error } = errorResponse;
        return throwError(error);
      })

    ).toPromise();
  }

  resetPassword( email: string ): Promise<any> {
    return this.http.post(`${environment.apiUrl}/api/user/newpassword`, 
    email
    ).pipe(

      tap (response => console.log(response)),

      catchError(errorResponse => {
        const error = errorResponse.statusText;
        return throwError(error);
      })
    ).toPromise();
  }

}
