import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { map, catchError, tap } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import User from '@models/user.model';
import PutUser from '@models/mocks/putUser.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private http: HttpClient
  ) { }

  getUser(): Promise<any> {
    let user: User;
    return this.http.get(`${environment.apiUrl}/api/user/path`, { observe: 'response' }).pipe(
      map((response: any) => {

        if (response.status >= 400) return throwError;

        user = response.body;
        return user;
      }),

      catchError(errorResponse => {
        const { error } = errorResponse.error || errorResponse.message;
        return throwError(error);
      })

    ).toPromise();
  }

  getUsers(): Promise<User[]> {
    return this.http.get<User[]>(`${environment.apiUrl}/api/user/`).pipe(
      catchError(errorResponse => {
        const { error } = errorResponse.error || errorResponse.message;
        return throwError(error);
      })

    ).toPromise();
  }
  patchRole(user): Promise<any> {
    let req = [{ op: "replace", path: "/role", value: user.role }];
    return this.http.patch(`${environment.apiUrl}/api/user/${user.id}`, req).pipe(
      catchError(errorResponse => {
        const { error } = errorResponse.error || errorResponse.message;
        return throwError(error);
      })
    ).toPromise();
  }
  patchUser(patchList: any, id: number): Promise<any> {
    return this.http.patch(`${environment.apiUrl}/api/user/${id}`,
      patchList
    ).pipe(
      catchError(errorResponse => {
        const { error } = errorResponse;
        return throwError(error);
      })
    ).toPromise();
  }

  putUser(userObj: PutUser, id: number): Promise<any> {
    return this.http.put(`${environment.apiUrl}/api/user/${id}`,
      userObj, { observe: 'response' }
    ).pipe(
      tap(res => console.log(res.status)),
      catchError(errorResponse => {
        const { error } = errorResponse;
        return throwError(error);
      })
    ).toPromise();
  }

  changePassword(id: number, newPassword: string, oldPassword: string): Promise<any> {
    return this.http.post(`${environment.apiUrl}/api/user/${id}/password`,
      {
        "oldPassword": oldPassword,
        "newPassword": newPassword
      }).pipe(
        catchError(errorResponse => {
          const { error } = errorResponse.message || errorResponse.error;
          return throwError(error);
        })
      ).toPromise();
  }

}
