import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  constructor() { }

  save(role: string, token: string): void { //send in role param when api is ready
    sessionStorage.setItem('token', token);
    sessionStorage.setItem('role', role);
  }

  get(): string | boolean {
    return sessionStorage.getItem('token') || false;
  }

  getContributor(): boolean {

    return sessionStorage.getItem('role') ? sessionStorage.getItem('role').includes('Contributer') : false;
  }

  getAdmin(): boolean {
    return sessionStorage.getItem('role') ? sessionStorage.getItem('role').includes('Admin') : false;
  }

  logOut(): void {
    sessionStorage.clear();
  }

}
