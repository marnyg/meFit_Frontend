import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { userList } from '@models/mocks/user.mock';
import { map, catchError, tap } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { environment } from '../../../environments/environment';
import Gender from '@models/gender.model';
import User from '@models/user.model';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  constructor(
    private http: HttpClient
  ) { }

  getUsers(): User[] {
    return userList;
  }

  getGendersList(): Promise<Gender[]>{
    return this.http.get(`${environment.apiUrl}/api/genders`).pipe(
      map((response: Gender[]) => {
        return response
      }),
      catchError(errorResponse => {
        const { error } = errorResponse;
        return throwError(error);
      })
    ).toPromise();
  }
}
