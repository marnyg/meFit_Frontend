import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import Program from '@models/program.model';
import { throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProgramsService {

  constructor(private http: HttpClient) { }


  public getProgramById(id: Number): Promise<Program> {
    return this.http.get<Program>(`${environment.apiUrl}/api/programs/${id}`).pipe(

      map((response: Program): any => {
        return response;
      }),

      catchError(errorResponse => {
        return throwError(errorResponse);
      })
    ).toPromise<Program>();
  }

  public getPrograms(): Promise<Program[]> {
    return this.http.get<Program[]>(`${environment.apiUrl}/api/programs`).pipe(

      map((response: Program[]): any => {
        return response;
      }),

      catchError(errorResponse => {
        return throwError(errorResponse);
      })
    ).toPromise<Program[]>();
  }

  public postProgram(program: any): Promise<any> {
    return this.http.post(`${environment.apiUrl}/api/programs`,
    program
    ).pipe(
      
      catchError(errorResponse => {
        const error = errorResponse.statusText;
        return throwError(error);
      })
    ).toPromise();
  }

}
