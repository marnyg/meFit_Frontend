import { Injectable } from '@angular/core';
import Workout from '@models/workout.model';
import * as workoutMocks from '@models/mocks/workout-mocks';
import { catchError, map } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class WorkoutsService {
  constructor(private http: HttpClient) { }

  public getWorkoutById(id: Number): Promise<Workout> {
    return this.http.get<Workout>(`${environment.apiUrl}/api/workouts/${id}`).pipe(

      catchError(errorResponse => {
        return throwError(errorResponse);
      })
    ).toPromise<Workout>();
  }

  public getWorkouts(): Promise<Workout[]> {
    return this.http.get<Workout[]>(`${environment.apiUrl}/api/workouts`).pipe(

      map((response: Workout[]): any => {
        return response;
      }),

      catchError(errorResponse => {
        return throwError(errorResponse);
      })
    ).toPromise<Workout[]>();
  }

  public postWorkout(workout: any): Promise<any> {
    return this.http.post(`${environment.apiUrl}/api/workouts`,
    workout
    ).pipe(
      
      catchError(errorResponse => {
        const error = errorResponse.statusText;
        return throwError(error);
      })
    ).toPromise();
  }

}


