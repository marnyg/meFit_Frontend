import { Injectable } from '@angular/core';
import * as exerciseMocks from '@models/mocks/exercise-mocks';
import Exercise from '@models/exercise.model';

import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { catchError, map, tap } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})


export class ExercisesService {
  constructor(private http: HttpClient) { };

  public getExerciseById(id: Number): Exercise {
    return exerciseMocks.singleExercise;
  }

  public getExercises(): Promise<Exercise[]> {
    return this.http.get<Exercise[]>(`${environment.apiUrl}/api/exercises`).pipe(

      catchError(errorResponse => {
        return throwError(errorResponse);
      })
    ).toPromise<Exercise[]>();
  }

  public postExercise(exercise: any): Promise<any> {
    return this.http.post(`${environment.apiUrl}/api/exercises`,
    exercise
    ).pipe(
      
      catchError(errorResponse => {
        const error = errorResponse.statusText;
        return throwError(error);
      })
    ).toPromise();
  }

}
