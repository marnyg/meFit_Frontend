import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import Category from '@models/category.model';
import { throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private http: HttpClient) { }

  public getCategories(): Promise<Category[]> {
    return this.http.get<Category[]>(`${environment.apiUrl}/api/categories`).pipe(

      map((response: Category[]): any => {
        return response;
      }),

      catchError(errorResponse => {
        return throwError(errorResponse);
      })
    ).toPromise<Category[]>();
  }

}
