import { HttpClient } from '@angular/common/http';
import { InterpolationConfig } from '@angular/compiler';
import { Injectable } from '@angular/core';
import Goal from '@models/goal.model';
import goalList from '@models/mocks/goal-mocks';
import { throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class GoalsService {
  constructor(private http: HttpClient) { }


  public getGoalByGoalId(id: Number): Promise<Goal> {
    return this.http.get<APIGoal>(`${environment.apiUrl}/api/goals/${id}`).pipe(

      map((response: APIGoal): Goal => {
        response.startDate = new Date(response.startDate);
        response.program = response.goalProgram;
        return response;
      }),

      catchError(errorResponse => {
        throw (errorResponse);
        return throwError(errorResponse);
      })
    ).toPromise<Goal>();
  }

  public getGoalsByUserid(id: Number): Promise<Goal[]> {
    return this.http.get<APIGoal[]>(`${environment.apiUrl}/api/goals/byprofileid/${id}`).pipe(

      map((response: APIGoal[]): Goal[] => {
        response.forEach(goal => {
          goal.startDate = new Date(goal.startDate);
          goal.program = goal.goalProgram;
        });
        console.log(response);
        return response;
      }),

      catchError(errorResponse => {
        throw (errorResponse);
        return throwError(errorResponse);
      })
    ).toPromise<Goal[]>();
  }

  public getAllGoals(id: Number): Goal[] {
    return goalList;
  }

  public postGoal() {
    // let goal = {
    //   profileId: 18,
    //   goalProgramId: 1,
    //   goalWorkouts: [
    //     {
    //       workoutId: 1,
    //       daysFromStart: 3
    //     }
    //   ],
    //   startDate: new Date()
    // };
    // let goal2 = {
    //   "profileId": 18,
    //   "goalProgramId": 1,
    //   "goalWorkouts": [
    //     {
    //       "workoutId": 1,
    //       "daysFromStart": 3
    //     }
    //   ],
    //   "startDate": "2020-10-19T08:07:45.422Z"
    // };
    // this.http.post(`${environment.apiUrl}/api/goals`, goal2).toPromise().then(
    //   p => { console.log(p); }
    // ).catch(e => console.log);
  }

}

interface APIGoal extends Goal {
  goalProgram: any;
}