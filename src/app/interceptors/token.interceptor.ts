import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { SessionService } from '../services/user/session.service';

const PUBLIC_URLS = ['/api/user/login', '/api/user/register'];

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor(
    private session: SessionService
  ) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    
    if(this.requireTokenHeader( request.url )){
      request = request.clone( this.createAuthHeader() );
    }

    return next.handle(request);
  }

  private createAuthHeader(): any {
    return{
      setHeaders: {
        'Authorization': 'Bearer ' + this.session.get()
      }
    }
  }

  private requireTokenHeader(url: string): boolean {
    return PUBLIC_URLS.includes( url.replace( environment.apiUrl, '' )) == false;
  }
}
