import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';

// Components
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { ExercisesComponent } from './pages/exercises/exercises.component';
import { UserProfileComponent } from './pages/user-profile/user-profile.component';
import { PersonalInformationComponent } from './components/profile/personal-information/personal-information.component';
import { UserMetricsComponent } from './components/profile/user-metrics/user-metrics.component';
import { MedicalInformationComponent } from './components/profile/medical-information/medical-information.component';
import { UserSettingsComponent } from './components/profile/user-settings/user-settings.component';
import { ProgramsComponent } from './pages/programs/programs.component';
import { WorkoutsComponent } from './pages/workouts/workouts.component';
import { ExerciseDetailsComponent } from './components/exercises/exercise-details/exercise-details.component';
import { ExercisesListComponent } from './components/exercises/exercises-list/exercises-list.component';
import { WorkoutDetailsComponent } from './components/workout/workout-details/workout-details.component';
import { WorkoutListComponent } from './components/workout/workout-list/workout-list.component';
import { ProgramsListComponent } from './components/programs/programs-list/programs-list.component';
import { GoalsComponent } from './pages/goals/goals.component';
import { GoalStatusComponent } from './components/goals/goal-status/goal-status.component';
import { GoalListComponent } from './components/goals/goal-list/goal-list.component';
import { ProgramDetailsComponent } from './components/programs/program-details/program-details.component';
import { GoalWorkoutsComponent } from './components/goals/goal-workouts/goal-workouts.component';
import { CreateGoalComponent } from './components/goals/create-goal/create-goal.component';
import { ProgramWorkoutListComponent } from './components/programs/program-workout-list/program-workout-list.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { WeeklyOverviewComponent } from './components/dasboard/weekly-overview/weekly-overview.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { AddExerciseComponent } from './components/exercises/add-exercise/add-exercise.component';
import { SelectListComponent } from './components/shared/select-list/select-list.component';
import { AddWorkoutComponent } from './components/workout/add-workout/add-workout.component';
import { AdminComponent } from './pages/admin/admin.component';
import { AddProgramComponent } from './components/programs/add-program/add-program.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { TokenInterceptor } from './interceptors/token.interceptor';
import { ErrorPopupComponent } from './components/shared/error-popup/error-popup.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginComponent,
    WorkoutsComponent,
    RegisterComponent,
    ExercisesComponent,
    UserProfileComponent,
    PersonalInformationComponent,
    UserMetricsComponent,
    MedicalInformationComponent,
    UserSettingsComponent,
    ProgramsComponent,
    ExerciseDetailsComponent,
    ExercisesListComponent,
    WorkoutDetailsComponent,
    WorkoutListComponent,
    ProgramsListComponent,
    GoalsComponent,
    GoalStatusComponent,
    GoalListComponent,
    ProgramDetailsComponent,
    GoalWorkoutsComponent,
    CreateGoalComponent,
    ProgramWorkoutListComponent,
    NotFoundComponent,
    AddExerciseComponent,
    SelectListComponent,
    DashboardComponent,
    WeeklyOverviewComponent,
    AddWorkoutComponent,
    AdminComponent,
    AddProgramComponent,
    ErrorPopupComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    DragDropModule
  ],
  providers: [DatePipe,
  {
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptor,
    multi: true
  }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
