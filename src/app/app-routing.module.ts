import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { ExercisesComponent } from './pages/exercises/exercises.component';
import { UserProfileComponent } from './pages/user-profile/user-profile.component';

import { WorkoutsComponent } from './pages/workouts/workouts.component';
import { RegisterComponent } from './pages/register/register.component';
import { ProgramsComponent } from './pages/programs/programs.component';
import { GoalsComponent } from './pages/goals/goals.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { AuthGuard } from './guards/auth/auth.guard';
import { AccessGuard } from './guards/access/access.guard';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { AdminComponent } from './pages/admin/admin.component';
import { AuthorizationGuard } from './guards/auth/authorization.guard';
;

const routes: Routes = [

  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'exercises',
    component: ExercisesComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'profile',
    component: UserProfileComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'exercises',
    component: ExercisesComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'workouts',
    component: WorkoutsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'programs',
    component: ProgramsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'goals',
    component: GoalsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'admin',
    component: AdminComponent,
    canActivate: [AuthGuard, AuthorizationGuard]
  },
  {
    path: '404',
    component: NotFoundComponent
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/login'
  },
  { // Denne må være nederst
    path: '**',
    redirectTo: '/404'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
